const { decodeStream } = require('iconv-lite');
const csv = require('./lib/csv');
const ofx = require('./lib/ofx');

module.exports = run;


// function debugStream() {
//   const { Transform } = require('stream');
//   return new Transform({
//     objectMode: true,
//     write(obj, encoding, next) {
//       this.push(JSON.stringify(obj));
//       next();
//     }
//   });
// }

function run(from, to, options) {
  from
    .pipe(decodeStream('Windows-1250'))
    .pipe(csv())
    .pipe(ofx(options))
    .pipe(to);
}
