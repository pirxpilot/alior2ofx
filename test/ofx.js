const ofx = require('../lib/ofx');

const credit = {
    'Data księgowania': '20180101',
    'Data transakcji': '20180102',
    'Nadawca': 'Jan Kowalski',
    'Odbiorca': 'Piote Kowalski',
    'Tytuł płatności (linia 1)': 'desciption credit',
    'Tytuł płatności (linia 2)': '',
    'Tytuł płatności (linia 3)': '',
    'Tytuł płatności (linia 4)': '',
    'Opis transakcji': 'memo credit',
    'Kwota': '12,23',
    'Waluta': 'PLN',
    'Saldo po operacji': '153,23',
};

const debit = {
    'Data księgowania': '20180103',
    'Data transakcji': '20180103',
    'Nadawca': 'Jan Kowalski',
    'Odbiorca': 'Piote Kowalski',
    'Tytuł płatności (linia 1)': 'desciption debit',
    'Tytuł płatności (linia 2)': '',
    'Tytuł płatności (linia 3)': '',
    'Tytuł płatności (linia 4)': '',
    'Opis transakcji': 'memo debit',
    'Kwota': '-15,15',
    'Waluta': 'PLN',
    'Saldo po operacji': '170,30',
};

const OFX =
`OFXHEADER:100
DATA:OFXSGML
VERSION:102
SECURITY:NONE
ENCODING:UTF8
CHARSET:undefined
COMPRESSION:NONE
OLDFILEUID:NONE
NEWFILEUID:NONE

<OFX>
<SIGNONMSGSRQV1>
<SONRS>
<STATUS>
<CODE>0
<SEVERITY>INFO
</STATUS>
<LANGUAGE>ENG
<APPID>ALIOR2OFX
<APPVER>1
</SONRS>
</SIGNONMSGSRQV1>
<BANKMSGSRSV1>
<STMTTRNRS>
<TRNUID>0
<STATUS>
<CODE>0
<SEVERITY>INFO
</STATUS>
<STMTRS>
<CURDEF>USD
<BANKACCTFROM>
<BANKID>223344
<ACCTID>1234 5678
<ACCTTYPE>CHECKING
</BANKACCTFROM>
<BANKTRANLIST>
<DTSTART>20180101
<DTEND>20180103
<STMTTRN>
<TRNTYPE>CREDIT
<DTPOSTED>20180101
<TRNAMT>12.23
<FITID>ffd76331-cdbb-500e-a298-a4c19bc2ace5
<NAME>desciption credit
<MEMO>memo credit
</STMTTRN>
<STMTTRN>
<TRNTYPE>DEBIT
<DTPOSTED>20180103
<TRNAMT>15.15
<FITID>363e7175-8e17-58c4-8711-6e0f5f603d98
<NAME>desciption debit
<MEMO>memo debit
</STMTTRN>
</BANKTRANLIST>
<LEDGERBAL>
<BALAMT>170.30
<DTASOF>20180103
</LEDGERBAL>
</STMTRS>
</STMTTRNRS>
</BANKMSGSRSV1>
</OFX>
`;

describe('ofx', function () {
  it('should generate transactions', function (done) {
    const stream = ofx({
      routing: '223344',
      account: '1234 5678'
    });

    stream.on('data', (data) => {
      data.should.eql(OFX);
      done();
    });
    stream.on('error', done);

    stream.write(credit);
    stream.write(debit);
    stream.end();
  });
});
