[![NPM version][npm-image]][npm-url]
[![Build Status][build-image]][build-url]
[![Dependency Status][deps-image]][deps-url]
[![Dev Dependency Status][deps-dev-image]][deps-dev-url]

# alior2ofx

Converterts Aior CSV to OFX

## Install

```sh
$ npm install --save alior2ofx
```

## Usage

```
Usage: alior2ofx [OPTION]... [FILE]
Convert CSV file downloaded from Alior Bank to OFX file.

With no FILE read standard input.

      --account   account number to be used
      --routing   routing number to be used (default: ALBPPLPW)
      --type      account type (default: CHECKING)
  -o, --out       destination OFX file
      --verbose   display options and names of the files
  -h, --help
```


## License

MIT © [Damian Krzeminski](https://pirxpilot.me)

[npm-image]: https://img.shields.io/npm/v/alior2ofx.svg
[npm-url]: https://npmjs.org/package/alior2ofx

[build-url]: https://bitbucket.org/pirxpilot/alior2ofx/addon/pipelines/home#!/results
[build-image]: https://img.shields.io/bitbucket/pipelines/pirxpilot/alior2ofx.svg

[deps-image]: https://img.shields.io/david/pirxpilot/alior2ofx.svg
[deps-url]: https://david-dm.org/pirxpilot/alior2ofx

[deps-dev-image]: https://img.shields.io/david/dev/pirxpilot/alior2ofx.svg
[deps-dev-url]: https://david-dm.org/pirxpilot/alior2ofx?type=dev
