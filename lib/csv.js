const csv = require('csv-parser');

module.exports = createStream;


function createStream() {
  return csv({
    separator: ';'
  });
}

