const { Transform } = require('stream');
const ofx = require('ofx');
const uuid = require('uuid/v5');

module.exports = createStream;


const NAMESPACE = 'ae95707d-4624-4f1a-9b5c-e0b5e4ff15cf';

function getId({ date, amount, memo }) {
  return uuid([
    date, amount, memo
  ], NAMESPACE);
}

function createStream(options) {

  const transactions = [];
  const period = {
    start: '99999999',
    end: '00000000'
  };
  const balance = {
    amount: '',
    date: '00000000'
  };

  function transform(chunk, encoding, next) {
    const [ transaction, { date, amount } ] = createTransaction(chunk);
    if (date < period.start) {
      period.start = date;
    }
    if (date > period.end) {
      period.end = date;
    }
    if (date > balance.date) {
      balance.date = date;
      balance.amount = amount;
    }
    transactions.push(transaction);
    next();
  }

  function final(next) {
    const header = createHeader();
    const body = createBody(transactions, Object.assign({ balance }, options, period));
    this.push(ofx.serialize(header, body));
    next();
  }

  return new Transform({
    objectMode: true,
    transform,
    final
  });
}


function createTransaction(item) {
  let {
    'Data księgowania': date,
    // "Data transakcji":
    // "Nadawca":
    // "Odbiorca":,
    "Tytuł płatności (linia 1)": line1,
    "Tytuł płatności (linia 2)": line2,
    "Tytuł płatności (linia 3)": line3,
    "Tytuł płatności (linia 4)": line4,
    "Opis transakcji": memo,
    "Kwota": amount,
    // "Waluta": currency,
    "Saldo po operacji": saldo,
  } = item;

  let type = 'CREDIT';
  if (amount[0] === '-') {
    type = 'DEBIT';
    amount = amount.slice(1);
  }
  amount = amount.replace(',', '.');

  const balance = {
    date: date,
    amount: saldo.replace(',', '.')
  };

  return [
    {
      TRNTYPE: type,
      DTPOSTED: date,
      TRNAMT: amount,
      FITID: getId({ date, amount, memo }),
      NAME: [line1, line2, line3, line4].join(' ').trim(),
      MEMO: memo,
    },
    balance
  ];
}

function createHeader() {
  return {
    OFXHEADER:'100',
    DATA:'OFXSGML',
    VERSION:'102',
    SECURITY:'NONE',
    ENCODING:'UTF8',
    // ENCODING:'USASCII',
    // CHARSET:'1252',
    COMPRESSION:'NONE',
    OLDFILEUID:'NONE',
    NEWFILEUID:'NONE'
  };
}


function createBody(transactions, { start, end, routing, account, type = 'CHECKING', balance }) {
  return {
    SIGNONMSGSRQV1: {
      SONRS: {
        STATUS: { CODE: 0, SEVERITY: 'INFO' },
        LANGUAGE: 'ENG',
        // FI: { ORG: financialInstituteOrg, FID: financialInstituteFID },
        APPID: 'ALIOR2OFX',
        APPVER: '1'
      }
    },
    BANKMSGSRSV1: {
      STMTTRNRS: {TRNUID: 0, STATUS: {CODE: 0, SEVERITY: 'INFO'},
            STMTRS: {
                CURDEF: 'USD',
                BANKACCTFROM: {
                    BANKID: routing,
                    ACCTID: account,
                    ACCTTYPE: type
                },
                BANKTRANLIST: { DTSTART: start, DTEND: end, STMTTRN: transactions },
                LEDGERBAL: { BALAMT: balance.amount, DTASOF: balance.date },
                // , AVAILBAL: { BALAMT: '', DTASOF: '' }
            }
        }
    }
  };
}
